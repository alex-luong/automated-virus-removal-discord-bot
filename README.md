# DiscordBot

**Description** 
  This is an automated link parsing and virus detection bot
  It will scan messages for links, test links against VirusTotal and then 
  delete messages if they're found to be malicious

## Installation

  **Add bot to discord server**
  [Bot Link](https://discordapp.com/api/oauth2/authorize?client_id=504822748678193194&permissions=92160&scope=bot)

  Please install Erlang and Elixir before proceeding

  **This commands must be ran in the same folder as the bot**
  Run: mix deps.get (This will get all needed dependancies)
  Run: iex -S mix (This will run the bot) 

  The bot will remain online as long as the command iex -S mix is running


Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/discord_bot](https://hexdocs.pm/discord_bot).

