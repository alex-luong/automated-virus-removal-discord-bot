defmodule Discord.Server do
  @moduledoc """
    Start a client process
  """
  use GenServer

  # Client side
  def start_link do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  # Callback Func
  def init(:ok) do
    {:ok, %{}}
  end

end
