defmodule Discord.EventHandler do
  @moduledoc """
    Event handler that deals with all discord gateway events
  """

  alias DiscordEx.RestClient.Resources.Channel
  alias VirusScanner, as: VS
  alias VirusScanner.LinkInput, as: VSL

  @doc """
    Handle when messages are created in current server
  """
  def handle_event({:message_create, payload}, state) do
    # Get message content
    contentPayload = payload.data["content"]

    # Check if the message is a valid link, and if it's a virus
    checkVirus = VSL.input(contentPayload)

    IO.inspect(checkVirus, label: "checkVirus: ")

    checkVirus =
      if checkVirus |> elem(0) != :false do
        VS.virus_total_scan(checkVirus)
      end


    # Get author map from message
    authorPayload = get_in(payload, [:data, "author"])

    # Current message isn't by a bot
    if checkBotExists(authorPayload) == nil do
      case contentPayload do
        "ping!" ->
          del_msg(payload, state)
          send_response("Pong!", payload, state)
        _ ->
          :ignore
      end

    end

    {:ok, state}
  end

  # Fallback Event Handler
  def handle_event({event, _payload}, state) do
    IO.puts "Received Event: #{event}"
    {:ok, state}
  end

  # Send Message
  def send_response(sendMsg, payload, state) do
    Channel.send_message(
      state[:rest_client],
      payload.data["channel_id"],
      %{content: sendMsg}
      )
  end

  # Delete message
  def del_msg(payload, state) do
    Channel.delete_message(
      state[:rest_client],
      payload.data["channel_id"],
      payload.data["id"])
  end

  # Check if the message is posted by a bot
  defp checkBotExists(authorMap) do
    if Map.has_key?(authorMap, "bot") do
      :ok
    end
  end
end
