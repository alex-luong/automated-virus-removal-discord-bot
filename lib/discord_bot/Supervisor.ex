defmodule Discord.Supervisor do
  @moduledoc """
    Start a supervisor that looks over the Discord.Server and
    DiscordEX Client processes, automatically rebooting them if they crash
  """

  use Supervisor

  def start_link do
    Supervisor.start_link(__MODULE__, :ok)
  end

  # Callback func
  def init(:ok) do
    botToken = Application.get_env(:discord_bot, :token)

    children = [
      worker(Discord.Server, []),
      worker(DiscordEx.Client, [%{token: botToken, handler: Discord.EventHandler}])
    ]
    options = [
      strategy: :one_for_one
    ]

    supervise(children, options)
  end

end
