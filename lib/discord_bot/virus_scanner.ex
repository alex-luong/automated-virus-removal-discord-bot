defmodule VirusScanner do
  alias VirusScanner.LinkInput, as: VSL

  @moduledoc """
    Program to take in a link, pass to virusTotal and output whether the link is a virus or not

    The program should take in discord messages and automatically flag viruses, removing the link and replacing with a warning
  """

  @doc """
    Takes a valid link and uses virus total to scan for viruses
  """
  def virus_total_scan(linkCheck) do
    IO.inspect(linkCheck, label: "linkCheck: ")

    # When the link is a valid site,
    # Send this link to virustotal and check the results
    if linkCheck |> elem(0) != :false do
      linkCheck = linkCheck |> elem(1)
      linkCheck = "http://" <> linkCheck
      IO.inspect(linkCheck, label: "linkCheck: ")

      virusTotalURL = "https://www.virustotal.com/ui/search?query=#{linkCheck}&relationships[url]=network_location,last_serving_ip_address&relationships[comment]=author,item"
      # Send a httpPoison get request
      getReq = HTTPoison.get!(virusTotalURL)
      jsonFile = handle_json(getReq)

      # IO.inspect(jsonFile, label: "jsonFile: ")
      if is_virus(jsonFile) == true do
        IO.puts "This is a virus"
        :true
      else
        IO.puts "Not a virus"
        :false
      end
    else
      IO.puts "This isn't a valid link"
      :error
    end
  end

  @doc """
    Parse the https GET Request body json file into a map
  """
  def handle_json(getReq) do
    getReq.body
      |> Poison.Parser.parse!
  end

  @doc """
    Extract last_analysis_stats from json get request
    and return true if virus, false if not
  """
  def is_virus(map) do
    # Manually navigate the DOM to get the last_analysis_stats
    siteResultMap = Map.get(map, "data")
    |> Enum.at(0)
    |> Map.get("attributes")
    |> Map.get("last_analysis_stats")

    posResult = siteResultMap["harmless"]
    negResult = siteResultMap["malicious"]
    totalRes = (posResult + negResult) * 0.1

    # Output is virus is the total malicious is >= total (posResult + negResult)
    if negResult >= totalRes do
      true
    else
      false
    end
  end

end
