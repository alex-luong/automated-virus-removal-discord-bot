defmodule VirusScanner.LinkInput do
  @moduledoc """
    Handles taking in messages or links and then sends it to VirusTotal
  """

  @doc """
    Takes in the input, in the form of a string or discord message
  """
  def input(strInput) do
    checkedStr = link_checking(strInput)

    # Check if the link is valid
    if checkedStr |> elem(0) == :ok do
      # Returns a valid link string
      IO.puts "link valid"
      IO.inspect(checkedStr, label: "checkedStr: ")

      {:ok, checkedStr|> elem(1)}
    else
      # Returns :error
      IO.puts "Not a valid link"
      {:false, []}
    end
  end

  @doc """
    Check if the message/string is actually a link, use URI or Regex

    Returns true if link, false if no
  """
  def link_checking(strInput) do
    # Checks if a message contains a link format
    # Extract link from the string
    linkFromMessage = regex_link_extraction(strInput)
    firstInputLink = linkFromMessage |> elem(1) |> Enum.at(0)

    IO.inspect(firstInputLink, label: "firstInputLink: ")
    IO.inspect(linkFromMessage |> elem(0), label: "linkMessage elem0: ")

    cond do
      linkFromMessage |> elem(0) == :false ->
        {:false, []}
      # Check if the link is a valid string format, check if the link is a string
      linkFromMessage |> elem(0) == :ok ->
        # Remove the http from link
        regexHttp = ~r/[a-zA-Z\d]+\.[a-zA-Z]+[\d\w#!:.?+=&%@!\-\/]*/
        firstInputLink = Regex.run(regexHttp, firstInputLink)

        firstInputLink =
          if firstInputLink != nil do
            Enum.at(firstInputLink, 0)
          end
        IO.inspect(firstInputLink, label: "firstInputLink cond: ")



        # Check if the link is valid using gethostbyname
        # Current string doesn't work
        validLink = :inet.gethostbyname(String.to_charlist( firstInputLink))
        IO.inspect(validLink, label: "ValidLink: ")

        # Check if link is valid
        if validLink != {:error, :nxdomain} do
          {:ok, firstInputLink}
        else
          {:false, []}
        end
      # else, return false
      true ->
        {:false, []}
    end
  end

  @doc """
  Takes in a message and extracts the link format from it
  returns a tuple with true and the link if there's a link
  otherwise, false and ""
  """
  def regex_link_extraction(strInput) do
    # Matches http:// or https://, then 0 or 1 www
    # Then matches 1 or more a-z A-Z, matches . and then matches any number of a-z A-Z
    # The last section matches anything after .DOMAIN/ including special characters
    regExpress = ~r/[a-zA-Z\d]+\.[a-zA-Z]+[\d\w#!:.?+=&%@!\-\/]*/

    # Runs the Regex against strInput, returns a list of all matches
    extractedLink = Regex.scan(regExpress, strInput)
    IO.inspect(Enum.empty?(extractedLink), label: "extractedLink: ")

    # Check if the list is empty, if so there's no link formats
    if !Enum.empty?(extractedLink) do
      # Remove the nested list
      extractedLink = List.flatten(extractedLink)
      IO.inspect(extractedLink, label: "inside extractedLink: ")

      # Add Http to the each item in the list
      extractedLink = Enum.map(extractedLink, fn( x) ->
        if Regex.match?(~r/^(www\.)/, x) == false do
          "http://www." <> x
        else
          "http://" <> x
        end
      end)


      # # Return true if there's a link in the message
      {:ok, extractedLink}
    else
      # "Return" false
      {:false, []}
    end
  end

  @doc """
  Check if a string has a link formatting and is correct according to URI
  """
  def checkURI(inputLink) do
    uri = URI.parse(inputLink)
    # Check if the string is a valid link format
    case uri do
      %URI{scheme: nil} ->
        :error
      %URI{host: nil} ->
        :error
      uri -> {:ok, inputLink}
    end
  end

end
