defmodule DiscordBot do
  use Application

  @moduledoc """
    Run a discord bot, parse all messages sent with the current server and parse these messages for 
    malicious links. 
    Automatically delete messages containing malicious links
  """

  @doc """
    Automatically run this upon running mix
    Starts a Supervisor process
  """
  def start(_type, _args) do
    Discord.Supervisor.start_link
  end

end
